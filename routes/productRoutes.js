const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

//Route for adding a product
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	if (userData.isAdmin) {
		productController
			.addProduct(req.body)
			.then((resultFromController) => res.send(resultFromController));
	} else {
		res.send("Unauthorized");
	}
});

//Route for retrieving all the products including inactive
router.get("/all", auth.verify, (req, res) => {
	productController
		.getAllProducts()
		.then((resultFromController) => res.send(resultFromController));
});

//Route for retrieving all the Active products
router.get("/", (req, res) => {
	productController
		.getAllActive()
		.then((resultFromController) => res.send(resultFromController));
});

//Route for retrieving all the Trending products
router.get("/trending", (req, res) => {
	productController
		.getAllTrending()
		.then((resultFromController) => res.send(resultFromController));
});

//Route for retrieving all the Best Seller products
router.get("/bestSellers", (req, res) => {
	productController
		.getAllBestSellers()
		.then((resultFromController) => res.send(resultFromController));
});

//Route for retrieving all Air Jordan 1
router.get("/airJordan1", (req, res) => {
	productController
		.getAllAirJordan1()
		.then((resultFromController) => res.send(resultFromController));
});

//Route for retrieving all Nike Air Force 1
router.get("/airForce1", (req, res) => {
	productController
		.getAllNikeAirForce1()
		.then((resultFromController) => res.send(resultFromController));
});

//Route for retrieving all Nike Dunk
router.get("/nikeDunk", (req, res) => {
	productController
		.getAllNikeDunk()
		.then((resultFromController) => res.send(resultFromController));
});

//Route for retrieving a specific product
router.get("/:productId", (req, res) => {
	// console.log(req.params);

	productController
		.getProduct(req.params)
		.then((resultFromController) => res.send(resultFromController));
});

//Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedProduct: req.body,
	};

	productController
		.updateProduct(data)
		.then((resultFromController) => res.send(resultFromController));

	// function sendResult (resultFromController) {
	// 	res.send(resultFromController)
	// }
});

//Archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};

	productController
		.archiveProduct(data)
		.then((resultFromController) => res.send(resultFromController));
});

//Unarchiving a product
router.put("/:productId/unarchive", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};

	productController
		.unarchiveProduct(data)
		.then((resultFromController) => res.send(resultFromController));
});

//Route for setting isTrending to true
router.put("/:productId/setTrending", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};

	productController
		.setTrendingTrue(data)
		.then((resultFromController) => res.send(resultFromController));
});

//Route for setting isTrending to false
router.put("/:productId/unsetTrending", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};

	productController
		.setTrendingFalse(data)
		.then((resultFromController) => res.send(resultFromController));
});

//Route for setting isBestSeller to true
router.put("/:productId/setBestSeller", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};

	productController
		.setBestSellerTrue(data)
		.then((resultFromController) => res.send(resultFromController));
});

//Route for setting isBestSeller to false
router.put("/:productId/unsetBestSeller", auth.verify, (req, res) => {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
	};

	productController
		.setBestSellerFalse(data)
		.then((resultFromController) => res.send(resultFromController));
});

module.exports = router;
