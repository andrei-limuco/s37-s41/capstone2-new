const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Name is required"],
	},
	category: {
		type: String,
		required: [true, "Category is required"],
	},
	description: {
		type: String,
		required: [true, "Description is required"],
	},
	price: {
		type: Number,
		required: [true, "Price is required"],
	},
	src: {
		type: String,
		required: [true, "Image is required"],
		unique: true,
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	isTrending: {
		type: Boolean,
		default: false,
	},
	isBestSeller: {
		type: Boolean,
		default: false,
	},
	createdOn: {
		type: Date,
		default: new Date(),
	},
});

module.exports = mongoose.model("Product", productSchema);
